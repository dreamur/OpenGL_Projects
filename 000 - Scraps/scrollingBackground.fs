// reference The Book of Shaders
// for more on this stuff
// http://thebookofshaders.com/07/

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);
    vec3 blu = vec3(0.0, 0.0, 1.0);
    color = blu;
    vec3 red = vec3(1.0, 0.1, 0.2);

    float f = step(abs(cos(st.x / 1.5)*sin(st.x*12. - u_time)), 0.35);
    f += step(abs(cos(st.y)*sin(st.y - 1.05)), 0.1);
    color *= vec3( 1.-smoothstep(f,f+0.02,1.0) ) + red;

    f = step(abs(cos(st.x)*sin(st.x*10. - u_time * 1.3)), 0.24);
    f += step(abs(cos(st.y)*sin(st.y -1.05)), 0.35);
    color *= vec3( 1.-smoothstep(f,f+0.02,1.0) ) + red;

    f = step(abs(cos(st.x)*sin(st.x*7. - u_time * 2.2)), 0.16);
    f += step(abs(cos(st.y)*sin(st.y -1.05)), 0.60);
    color *= vec3( 1.-smoothstep(f,f+0.02,1.0) ) + red;

    gl_FragColor = vec4(color, 1.0);
}

/*
=== reference key for later ===

f = step(abs(cos(st.x)*sin(st.x*w. - u_time * t)), w2);

... cos(st.x)                   ;# adjust denominator as necessary
... sin(...)
    ...(st.x * w - u_time * t)  ;# w is 'width' of pillars [ 4 <= w <= 20 for best ]
                                ;# t is time scalar [ |t| > 1.0 means faster ]
... step(... , w2)              ;# w2 also controls width & spacing of pillars
                                ;# [ 0 < w <= 0.7 ] for best results

f += step(abs(cos(st.y)*sin(st.y -h)), h2)

> this is same as above; just check y coords to 'chop' off the top
> there's also two ways to adjust height
  > use h or h2
  > h was always at 1.05
  > then then use h2 in step for the adjustments

*/
