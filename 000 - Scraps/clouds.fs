
// circle drawing concept discussed in the bookofshaders.com
// chapter 7 - shapes
vec3 smoothCircle(float lowBound, float upBound, float var)
{
    return vec3(smoothstep(lowBound, upBound, var));
}

vec3 cloud(vec2 pos, vec3 cloudColor, float xPos, float yPos)
{
    float pct = distance(pos,vec2(xPos, yPos));
    vec3 circ = step(0.5, smoothCircle(0.13, 0.15, pct));
    circ = step(vec3(0.5, 0.1, 0.4), mix(circ, cloudColor, .5));


    pct = distance(pos,vec2(xPos + 0.05, yPos + 0.08));
    circ *= step(0.5, smoothCircle(0.13, 0.14, pct));
    circ = step(vec3(0.5, 0.1, 0.4), mix(circ, cloudColor, .5));


    pct = distance(pos,vec2(xPos + 0.06, yPos - 0.08));
    circ *= step(0.5, smoothCircle(0.08, 0.12, pct));
    circ = step(vec3(0.5, 0.1, 0.4), mix(circ, cloudColor, .5));


    pct = distance(pos,vec2(xPos + 0.1, yPos));
    circ *= step(0.5, smoothCircle(0.13, 0.15, pct));
    circ = step(vec3(0.5, 0.1, 0.4), mix(circ, cloudColor, .5));

    return circ;
}

void main()
{
    vec2 st = gl_FragCoord.xy/u_resolution.xy;

    vec3 pCloudColor = vec3(.678, .847, .902);

    vec3 circ = cloud(st, pCloudColor, 0.3, 0.8);
    circ *= cloud(st, pCloudColor, 0.2, 0.1);
    circ *= cloud(st, pCloudColor, 0.8, 0.5);

    gl_FragColor = vec4(circ, 1.0 );
}
