precision mediump float;

/**************** circular fns ********/
float circularEaseIn (float x)
{
	return 1.0 - sqrt(1.0 - x * x);
}

float circularEaseOut (float x)
{
	return sqrt(1.0 - (1.0 - x)*(1.0 - x) );
}

float doubleCircleSeat (float x, float pa)
{
	float min_pa = 0.0;
	float max_pa = 1.0;
	float a = clamp(pa, min_pa, max_pa);

	float y = 0.0;
	if (x <= a)
		y = sqrt(a * a - (x-a) * (x-a));
	else
		y = 1.0 - sqrt((1.0-a) * (1.0-a) - (x-a) * (x-a));

	return y;
}

float doubleCircleSigmoid (float x, float pa)
{
	float min_pa = 0.0;
	float max_pa = 1.0;
	float a = clamp(pa, min_pa, max_pa);

	float y = 0.0;
	if (x <= a)
		y = a - sqrt(a * a - x * x);
	else
		y = a + sqrt((1.0-a) * (1.0-a) - (x-1.0) * (x-1.0));

	return y;
}

float doubleEllipticSeat (float x, float pa, float pb)
{
	float eps = 0.00001;
	float min_pa = 0.0 + eps;
	float max_pa = 1.0 + eps;
	float min_pb = 0.0;
	float max_pb = 1.0;

	float a = clamp(pa, min_pa, max_pa);
	float b = clamp(pb, min_pb, max_pb);

	float y = 0.0;
	if (x <= a)
		y = (b/a) * sqrt(a * a - (x-a) * (x-a));
	else
		y = 1.0 - ( (1.0-b) / (1.0-a) ) * sqrt( (1.0-a) * (1.0-a) - (x-a) * (x-a) );

	return y;
}

float doubleEllipticSigmoid (float x, float pa, float pb)
{
	float eps = 0.00001;
	float min_pa = 0.0 + eps;
	float max_pa = 1.0 - eps;
	float min_pb = 0.0;
	float max_pb = 1.0;

	float a = clamp(pa, min_pa, max_pa);
	float b = clamp(pa, min_pb, max_pb);

	float y = 0.0;
	if (x <= a)
		y = b * (1.0 - (sqrt(a * a - x * x) / a));
	else
		y = b + ( (1.0-b) / (1.0-a) ) * sqrt( (1.0-a)*(1.0-a) - (x-1.0)*(x-1.0) );

	return y;
}

/*******

note that

Double-Linear with Circular Fillet
Circular Arc Through a Given Point

currently do not work b/c GLSL does not
support global variables. a possible
workaraound will be explored in the future.

******/

float quadraticBezier (float x, float pa, float pb)
{
	float eps = 0.00001;
	float a = clamp(pa, 0.0, 1.0);
	float b = clamp(pb, 0.0, 1.0);
	if (a == 0.5)
		a += eps;

	float om2a = 1.0 - 2.0 * a;
	float t = (sqrt(a*a + om2a*x) - a) / om2a;
	float y = (1.0 - 2.0 * b) * (t * t) + (2.0 * b) * t;
	return y;
}

float slopeFromT (float t, float A, float B, float C)
{
	return 1.0 / (3.0 * A * t * t + 2.0 * B * t + C);
}

float xFromT (float t, float A, float B, float C, float D)
{
	return A * (t*t*t) + B * (t*t) + C * t + D;
}

float yFromT (float t, float E, float F, float G, float H)
{
	return E * (t*t*t) + F * (t*t) + G*t + H;
}

float cubicBezier (float x, float pa, float pb, float pc, float pd)
{
	float y0a = 0.00;
	float x0a = 0.00;
	float y1a = pb;
	float x1a = pa;
	float y2a = pd;
	float x2a = pc;
	float y3a = 1.00;
	float x3a = 1.00;

	float A =       x3a - 3.0 * x2a + 3.0 * x1a - x0a;
	float B = 3.0 * x2a - 6.0 * x1a + 3.0 * x0a;
	float C = 3.0 * x1a - 3.0 * x0a;
	float D =       x0a;

	float E =       y3a - 3.0 * y2a + 3.0 * y1a - y0a;
	float F = 3.0 * y2a - 6.0 * y1a + 3.0 * y0a;
	float G = 3.0 * y1a - 3.0 * y0a;
	float H =       y0a;

	float current = x;
	const int nRefinementIterations = 5;
	for (int i = 0; i < nRefinementIterations; i++)
	{
		float currentx = xFromT (current, A, B, C, D);
		float currentslope = slopeFromT (current, A, B, C);
		current -= (currentx - x) * (currentslope);
		current = clamp(current, 0.0, 1.0);
	}

	float y = yFromT (current, E, F, G, H);
	return y;
}

float plot(vec2 st, float pct)
{
	return smoothstep( pct-0.02, pct, st.y) - smoothstep( pct, pct+0.02, st.y);
}

void main()
{
	vec2 st = gl_FragCoord.xy/iResolution;
	float y;

	//y = circularEaseIn(st.x);
	//y = circularEaseOut(st.x);
	//y = doubleCircleSeat(st.x, .5);
	//y = doubleCircleSigmoid(st.x, .5);
	//y = doubleEllipticSeat(st.x, .5, .5);
	//y = doubleEllipticSigmoid(st.x, .8, .184);
	//y = quadraticBezier(st.x, .9, .0);
	y = cubicBezier(st.x, 0.457, 0.160, 0.250, 0.750);

	vec3 color = vec3(y);

	float pct = plot(st,y);
	color = (1.0-pct)*color+pct*vec3(0.0, 1.0,0.0);

	gl_FragColor = vec4(color, 1.0);
}
