//precision mediump float;

/*
	uniform vec2 iResolution
 	uniform vec2 iMouse
	uniform float iGlobalTime
*/

//#version 330 core
//uniform vec2 iResolution;
//uniform vec2 u_resolution;


// approximate cosine 'under the hood'
// http://www.flong.com/texts/code/shapers_poly/
float blinnWyvillCosineApproximation(float x)
{
	float x2 = x*x;
	float x4 = x2 * x2;
	float x6 = x4 * x2;

	float fa = ( 4.0 / 9.0 );
	float fb = ( 17.0 / 9.0 );
	float fc = ( 22.0 / 9.0 );

	float y = fa * x6 - fb*x4 + fc*x2;
	return y;
}

float doubleCubicSeat(float x, float pa, float pb)
{
	float epsilon = 0.00001;
	float min_param_a = 0.0 + epsilon;
	float max_param_a = 1.0 - epsilon;
	float min_param_b = 0.0;
	float max_param_b = 1.0;

	float a = min(max_param_a, max(min_param_a, pa));
	float b = min(max_param_b, max(min_param_b, pb));

	float y = 0.0;
	if (x <= a) {
		y = b - b * pow(1.0 - x/a, 3.0);
	} else {
		y = b + (1.0 - b) * pow((x - a) / (1.0 - a), 3.0);
	}
	return y;
}

float doubleCubicSeatWithLinearBlend(float x, float pa, float pb)
{
	float epsilon = 0.00001;
	float min_param_a = 0.0 + epsilon;
	float max_param_a = 1.0 - epsilon;
	float min_param_b = 0.0;
	float max_param_b = 1.0;

	float a = min(max_param_a, max(min_param_a, pa));
	float b = min(max_param_b, max(min_param_b, pb));

	b = 1.0 - b;

	float y = 0.0;
	if (x <= a) {
		y = b*x + (1.0-b) * a * (1.0 - pow(1.0 - x/a, 3.0));
	} else {
		y = b*x + (1.0-b) * (a + (1.0-a) * pow((x-a) / (1.0-a), 3.0));
	}
	return y;
}

float doubleOddPolynomialSeat(float x, float pa, float pb, float n)
{
	float epsilon = 0.00001;
	float min_param_a = 0.0 + epsilon;
	float max_param_a = 1.0 - epsilon;
	float min_param_b = 0.0;
	float max_param_b = 1.0;

	float a = min(max_param_a, max(min_param_a, pa));
	float b = min(max_param_b, max(min_param_b, pb));

	float p = 2.0*n + 1.0;

	float y = 0.0;
	if (x <= a) {
		y = b - b*pow(1.0-x/a, p);
	} else {
		y = b + (1.0-b) * pow( (x-a)/(1.0-a), p);
	}
	return y;
}

float doublePolynomialSigmoid(float x, float n)
{
	float y = 0.0;
	int N = int(n);

	if ( mod(n, 2.0) == 0.0)
	{
		if (x <= 0.5) {
			y = pow(2.0 * x, n) / 2.0;
		} else {
			y = 1.0 - pow( (2.0*x - 2.0), n) / 2.0;
		}
	}
	else {
		if (x <= 0.5) {
			y = pow(2.0 * x, n) / 2.0;
		} else {
			y = 1.0 + pow( (2.0*x-2.0), n) / 2.0;
		}
	}

	return y;
}

float quadraticThroughAGivenPoint (float x, float pa, float pb)
{
	float epsilon = 0.00001;
	float min_param_a = 0.0 + epsilon;
	float max_param_a = 1.0 - epsilon;
	float min_param_b = 0.0;
	float max_param_b = 1.0;

	float a = min(max_param_a, max(min_param_a, pa));
	float b = min(max_param_b, max(min_param_b, pb));

	float A = (1.0- b) / (1.0-a) - (b/a);
	float B = (A*(a*a)-b)/a;
	float y = A*(x*x) - B*x;
	y = min(1.0, max(0.0, y));

	return y;
}

float exponentialEasing (float x, float pa)
{
	float epsilon = 0.00001;
	float min_param_a = 0.0 + epsilon;
	float max_param_a = 1.0 - epsilon;
	float a = min(max_param_a, max(min_param_a, pa));
	float y = 0.0;

	if (a < 0.5)
	{
	 	a = 2.0 * a;
		y = pow(x, a);
	}
	else
	{
		a = 2.0 * (a - 0.5);
		y = pow(x, 1.0 / (1.0 - a));
	}
	return y;
}

float doubleExponentialSeat (float x, float pa)
{
	float epsilon = 0.00001;
	float min_p_a = 0.0 + epsilon;
	float max_p_a = 1.0 - epsilon;
	float a = min(max_p_a, max(min_p_a, pa));

	float y = 0.0;
	if (x <= 0.5)
		y = pow(2.0 * x, 1.0 - a) / 2.0;
	else
		y = (2.0 - pow(2.0 * (1.0-x), 1.0 - a) )/ 2.0;
	return y;
}

float doubleExponentialSigmoid (float x, float pa)
{
	float eps = 0.00001;
	float min_pa = 0.0 + eps;
	float max_pa = 1.0 - eps;
	float a = min(max_pa, max(min_pa, pa));
	a = 1.0 - a; // for sensible results

	float y = 0.0;
	if (x <= 0.5)
		y = pow(2.0 * x, 1.0 / a) / 2.0;
	else
		y = (2.0 - pow(2.0 * (1.0 - x), 1.0/a) )/ 2.0;
	return y;
}

float logisticSigmoid (float x, float pa)
{
	float eps = 0.00001;
	float min_pa = 0.0 + eps;
	float max_pa = 1.0 + eps;
	float a = clamp(pa, min_pa, max_pa);
	a = 1.0 / (1.0 - a) - 1.0;

	float A = 1.0 / (1.0 + exp(0.0 - ((x-0.5) * a * 2.0)));
	float B = 1.0 / (1.0 + exp(a));
	float C = 1.0 / (1.0 + exp(0.0 - a));
	float y = (A - B) / (C - B);
	return y;
}

float plot(vec2 st, float pct)
{
	return smoothstep( pct-0.02, pct, st.y) - smoothstep( pct, pct+0.02, st.y);
}

void main()
{
	vec2 st = gl_FragCoord.xy/iResolution;
  float y;

	y = doubleCubicSeat(st.x, 0.640, 0.827);
	y = blinnWyvillCosineApproximation(st.x);
	y = doubleCubicSeatWithLinearBlend(st.x, .640, .827);
	y = doubleOddPolynomialSeat(st.x, 0.75, 0.75, 0.9);
	y = doublePolynomialSigmoid(st.x, 2.0);
	y = quadraticThroughAGivenPoint(st.x, 0.500, .607);
	y = exponentialEasing(st.x, 0.28);
	y = doubleExponentialSeat(st.x, 0.50);
	y = doubleExponentialSigmoid(st.x, 0.426);
	y = logisticSigmoid(st.x, 0.725);


	vec3 color = vec3(y);

	float pct = plot(st,y);
	color = (1.0-pct)*color+pct*vec3(0.0, 1.0,0.0);

	gl_FragColor = vec4(color, 1.0);
}
