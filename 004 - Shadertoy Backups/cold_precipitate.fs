#define PI 3.14159265359
#define TWO_PI 6.28318530718

vec3 snowfl(vec2 coordSpace)
{
    vec3 clr = vec3(0.0);

    float r = length(coordSpace)*10.0;
    float a = atan(coordSpace.y,coordSpace.x);
    float f = cos(a*8.);
    clr = vec3( 1.-smoothstep(f,f+.02,r) );

    r = length(coordSpace)*10.5;
    a = atan(coordSpace.y,coordSpace.x);
    f = abs(cos(a*6.))*.5+.3;

    clr -= vec3( 1.-smoothstep(f,f+0.02,r) );

    return clr;
}


void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 st = fragCoord/iResolution.xy;
    vec3 color = vec3(0.0);

    float timeX = sin( iTime ) * 0.05;

    vec2 pos = vec2( timeX + .3, -tan( iTime * .8 )  )-st;
    color += snowfl(pos);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .5, -tan( iTime * .5 ) )-st;
    color += snowfl(pos * 3.);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .8, -tan( iTime * .3 ) )-st;
    color += snowfl(pos * 3.);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .2, -tan( iTime * .71 ) )-st;
    color += snowfl(pos * 3.);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .7, -tan( iTime * .2 ) )-st;
    color += snowfl(pos * 1.2);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .4, -tan( iTime * .15 ) )-st;
    color += snowfl(pos * 1.2);
    color += snowfl(pos * 2.);

    pos = vec2( timeX + .1, -tan( iTime * .4221 ) )-st;
    color += snowfl(pos * 2.);
    color += snowfl(pos * 2.);

    color = 1. - color;
    color *= color * ( 1. - vec3(.529, .808, .922) );
    color = 1. - color;

    fragColor = vec4(color,1.0);
}
