vec3 rect(vec2 coordSpace, vec2 dim, vec2 offset)
{
    vec3 color = vec3(0.0);

    float l = offset.x;
    float r = offset.x + dim.x;
    float b = offset.y;
    float t = offset.y + dim.y;

    vec2 tmp = step( vec2(l, 0.0), coordSpace );
    float pct = tmp.x * tmp.y;
    color += vec3(pct);
    
    tmp = step( vec2(r, 0.0), coordSpace);
    pct = tmp.x * tmp.y;
    color -= vec3(pct);

    tmp = step( vec2(0.0, b), coordSpace);
    pct = tmp.x * tmp.y;
    color -= 1.0 - vec3(pct);

    tmp = step( vec2(0.0, t), coordSpace);
    pct = tmp.x * tmp.y;
    color -= vec3(pct);

    return color;
}

vec3 invert(vec3 c)
{
    return 1.0 - c;
}

vec3 merge(vec3 src, vec3 x)
{
 	return src *= x;
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    // Normalized pixel coordinates (from 0 to 1)
    vec2 st = fragCoord/iResolution.xy;

    vec3 color = vec3(0.0);

    color = merge(invert(color), rect( st, vec2(.8,.8), vec2(.1, .1) ) );
    color = merge(color, invert(rect( st, vec2(.3,.25), vec2(.35, .1) )) );

    color = merge(color, invert(rect( st, vec2(.08,.14), vec2(.21, .53) )) );
    color = merge(color, invert(rect( st, vec2(.08,.14), vec2(.46, .53) )) );
    color = merge(color, invert(rect( st, vec2(.08,.14), vec2(.71, .53) )) );


    color = merge(color, invert(rect( st, vec2(.15,.1), vec2(.7, .83) )) );
    color = merge(color, invert(rect( st, vec2(.15,.1), vec2(.4, .83) )) );
    color = merge(color, invert(rect( st, vec2(.15,.1), vec2(.1, .83) )) );

    // Output to screen
    fragColor = vec4(color,1.0);
}
