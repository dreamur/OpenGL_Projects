#define PI 3.14159265358

// https://thebookofshaders.com/10/
float rand (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

float rect2(vec2 coordSpace, vec2 dim, vec2 offset)
{
    vec2 tmp = smoothstep( dim, dim + .02, coordSpace - offset);
    float pct = tmp.x * tmp.y;

    tmp = smoothstep( dim, dim + .02, 1. - coordSpace + offset);
    pct *= tmp.x * tmp.y;

    return pct;
}


// https://thebookofshaders.com/07/
float circle(in vec2 _st, in float _radius){
    vec2 dist = _st-vec2(0.5);
	return 1.-smoothstep(_radius-(_radius*0.05),
                         _radius+(_radius*0.1),
                         dot(dist,dist)*3.7);
}


// https://thebookofshaders.com/06/
// Also...
//  Function from Iñigo Quiles
//  https://www.shadertoy.com/view/MsS3Wc
vec3 hsb2rgb( in vec3 c ){
    vec3 rgb = clamp(abs(mod(c.x*6.0+vec3(0.0,4.0,2.0),
                             6.0)-3.0)-1.0,
                     0.0,
                     1.0 );
    rgb = rgb*rgb*(3.0-2.0*rgb);
    return c.z * mix(vec3(1.0), rgb, c.y);
}

mat2 rotate(float angl)
{
 	return mat2( cos(angl * PI), -sin(angl * PI),
                 sin(angl * PI),  cos(angl * PI) );
}

float gridLine2(vec2 coordSpace, vec2 rectSize, vec2 rectOffset, float rotAngle)
{

    coordSpace 	= rotate(rotAngle) * coordSpace;
    rect2(coordSpace, rectSize, rectOffset);

    return rect2(coordSpace, rectSize, rectOffset);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 st = fragCoord/iResolution.xy;

    st -= 0.5;
    st.x *= iResolution.x/iResolution.y;
    st += 0.5;

    vec3 col = vec3(0.0);

    vec3 backCol = vec3(0.0);
    backCol = vec3(0.,0.,.153);

    float upRight;

    float vertThickness = 2.0;
    float horizThickness = 0.75;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.0, -0.6), 0.0) * vertThickness;	// << completely vertical (center line)

    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.27, -0.45), -.11) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(0.22, -0.8), .11) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.31, -1.05), .20) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.50, -0.5), -.20) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.34, -1.05), .25) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.64, -.5), -.25) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.350, -1.2), .29) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.745, -.60), -.29) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.350, -1.53), .32) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.815, -.690), -.32) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.305, -1.7), .36) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.88, -0.80), -.36) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.29, -1.83), .38) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.925, -.91), -.38) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.29, -1.93), .39) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.955, -1.002), -.39) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(0.31, -2.03), .395) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-0.990, -1.11), -.395) * vertThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.08, -0.55), .5) * vertThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.08,-1.45), .5) * vertThickness;	// << horizontal (horizon line)


    backCol += gridLine2(st, vec2(.49,.02), vec2(-.49, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.49, -0.55), .5) * horizThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.34, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.34, -0.55), .5) * horizThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.25, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.25, -0.55), .5) * horizThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.18, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.18, -0.55), .5) * horizThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.13, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.13, -0.55), .5) * horizThickness;

    backCol += gridLine2(st, vec2(.49,.02), vec2(-.10, -1.45), .5) * horizThickness;
    backCol += gridLine2(st, vec2(.49,.02), vec2(-.10, -0.55), .5) * horizThickness;


    for (int i = 0; i < 30; ++i)
    {
    	backCol += gridLine2(st, vec2(.4825,.4825), vec2(0.0 - -rand(st.xx), 0.0 - -rand(st.yy) -1.0 ), .5);
        backCol += gridLine2(st, vec2(.4825,.4825), vec2(0.0 - -rand(st.xx), 0.0 - -rand(st.yy) -2.0 ), .5);
    }


    //-----------------------------// << color the sun
    vec3 colSun;
    colSun = hsb2rgb(vec3(st.y/2.0 -.10, 1.0, st.x+.8 ));

    colSun += vec3(.75, 0.125, 0.);
    colSun = mix(colSun, vec3(0.1, .1, 0.1), .33);


    float cir = circle(vec2(st.x, st.y) , 0.56);
    colSun *= vec3(cir);

    float lin = rect2(vec2(st.x, st.y), vec2(.05, 0.480), vec2(0.0, -.05) );
    colSun *= 1.0 - vec3(lin);

    lin = rect2(vec2(st.x, st.y), vec2(.05, 0.47), vec2(0.0, -.132) );
    colSun *= 1.0 - vec3(lin);

    lin = rect2(vec2(st.x, st.y), vec2(.05, 0.47), vec2(0.0, -.22) );
    colSun *= 1.0 - vec3(lin);

    lin = rect2(vec2(st.x, st.y), vec2(.04, 0.468), vec2(0.0, -.295) );
    colSun *= 1.0 - vec3(lin);

    lin = rect2(vec2(st.x, st.y), vec2(.04, 0.468), vec2(0.0, -.36) );
    colSun *= 1.0 - vec3(lin);
    //-------------------------------//

    col = colSun;


    if (step(vec3(.000005), colSun) == vec3(0.0))
        //colSun += vec3(.235, .0, 0.471);
        col += backCol - vec3(0.5, 1.0, 0.10);
    //col = colSun;

    fragColor = vec4(col,1.0);
}
