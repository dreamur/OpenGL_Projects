#define PI 3.14159265

// concept from "The Book of Shaders"
//  Chapter 7: Shapes
//  https://thebookofshaders.com/07/
float lamp( in vec2 _st, in float edges )
{
    float a = atan(_st.x, _st.y) + .2;
    float b = PI * 2.0 / edges;

    return cos( floor(0.5 + a / b) * b - a ) *
           length( _st * 0.83 / cos(length(_st * 2.75)) );
}

vec2 rotate( in vec2 _st, in float angle )
{
	return mat2( cos(angle * PI), -sin(angle * PI),
                 sin(angle * PI), cos(angle * PI) ) * _st;
}

float circle( in vec2 _st, in float radius, in float inner, in float outer )
{
	vec2 dist = _st - vec2(0.5);
    return 1.0 - smoothstep( radius * inner, radius * outer, dot(dist, dist) * 4.0 );
}

float rect( in vec2 _st, in vec2 xCoords, in vec2 yCoords )
{
    vec2 bl = step( vec2(xCoords.x, yCoords.x), _st );
    vec2 tr = step( vec2(xCoords.y, yCoords.y), 1.0 - _st );

	return bl.x * bl.y * tr.x * tr.y;
}

float smoothRect( in vec2 _st, in vec2 xCoords, in vec2 yCoords, in vec2 shading )
{
    vec2 bl = smoothstep( vec2(xCoords.x, yCoords.x) * shading.x, vec2(xCoords.x, yCoords.x) * 1.01, _st );
    vec2 tr = smoothstep( vec2(xCoords.y, yCoords.y) * shading.y, vec2(xCoords.y, yCoords.y) * 1.01, _st );

	return bl.x * bl.y * tr.x * tr.y;
}

// concept from "The Book of Shaders"
//  Chapter 7: Shapes
//  https://thebookofshaders.com/07/
float rect2( in vec2 _st, in vec2 scale, in vec2 offset, in float roundedPct )
{
	float tmp = length(max( abs(_st * scale - offset) - 0.45  , 0.0));
  	return 1.0 - step(roundedPct, tmp);
}

// from "The Book of Shaders"
//  Chapter 11: Noise
//  https://thebookofshaders.com/11/
float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))
                 * 43758.5453123);
}

// from "The Book of Shaders"
//  Chapter 11: Noise
//  https://thebookofshaders.com/11/
float noise (in vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f*f*(3.0-2.0*f);

    return mix(a, b, u.x) +
            (c - a)* u.y * (1.0 - u.x) +
            (d - b) * u.x * u.y;
}


// concept from "The Book of Shaders"
//  Chapter 7: Shapes
//  https://thebookofshaders.com/07/
float heart( in vec2 _st, in float innerBlur, in float outerBlur )
{

	float r = length(_st) * 2.0;
    float a = atan(_st.x, _st.y);

    float pct = cos(r * a);
    	  pct = cos( pct * cos( a * 0.99 * (pct * r - 1.45) ) * sin(a * 2.2 - a * 3.6) );

    return 1.0 - smoothstep(pct - innerBlur, pct + outerBlur, r);
}

//----------------------------------------------------------//

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 st    = fragCoord / iResolution.xy;

    	 st   -= 0.5;
    	 st.x *= iResolution.x / iResolution.y;
    	 st   += 0.5;


    float pct, res, sinusoidalTime = sin(iTime), tmp1;
    vec2 tmp2  = vec2(0.0);
    vec3 col   = vec3(0.047, 0.22, 0.341), tmp3 = vec3(0.0);

    	 // jitter
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );


         // wall design
    	 res = 0.0, pct = 0.0;
    	 for(int i = 0; i < 5; i++)
         {
             pct += rect( tmp2 + vec2(0.75 - res, 0.0), vec2(0.491), vec2(0.0) );
             res += 0.37;
         }
    	 col  = mix(col, vec3(0.0, 0.45, 0.8), pct);

    	 res = 0.0, pct = 0.0;
    	 for(int i = 0; i < 8; i++)
         {
             pct += rect( tmp2 + vec2(0.76 - res, 0.0), vec2(0.4965), vec2(0.0) );
             res += 0.28 * 0.78;
         }
    	 col  = mix(col, vec3(0.0, 0.45, 0.8), pct);

    	 res = 0.0, pct = 0.0;
    	 for(int i = 0; i < 5; i++)
         {
             pct += rect( tmp2 + vec2(0.624 - res, 0.0), vec2(0.4952), vec2(0.0) );
             res += 0.32 * 1.02;
         }
    	 col  = mix(col, vec3(0.494, 0.78, 1.), pct);


    	 // blood splatter(s)
    	 pct   = smoothstep(0.68, 0.68, noise(tmp2.yx * 31.0));
    	 tmp3  = mix(col, vec3(0.95, 0.0, 0.0), pct);

    	 pct  *= smoothstep(0.62, 0.62, noise(tmp2 * 35.0 + tmp2 * 28.0));
    	 col   = mix(col, tmp3, pct);


         // bullet holes  - add variance to y pos
    	 res = -0.75;
    	 pct = 0.0;
    	 tmp1 = 0.21;
         pct += circle(tmp2 - vec2(res, tmp1), 0.0007, 0.35, 1.1);
    	 res += 0.56;
    	 tmp1 += 0.17;
    	 pct += circle(tmp2 - vec2(res, tmp1), 0.0007, 0.35, 1.1);
    	 res += 0.27;
		 tmp1 -= 0.24;
		 pct += circle(tmp2 - vec2(res, tmp1), 0.0007, 0.35, 1.1);
    	 res += 0.6;
		 tmp1 += 0.31;
		 pct += circle(tmp2 - vec2(res, tmp1), 0.0007, 0.35, 1.1);
    	 col   = mix(col, vec3(0.0), pct);


    	 // picture
    	 tmp2  = fragCoord / iResolution.xy;
    	 tmp2 -= 0.5;
    	 tmp2.y += mod(tmp2.y + iTime * 2.5 - 4.5, 12.5);
    	 tmp2.x *= iResolution.x / iResolution.y;
    	 tmp2  = rotate(tmp2, -0.08);
    	 tmp2 += 0.5;
    	 tmp2  = tmp2 + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 ) - vec2(0.25, 0.9);


     	 pct   = rect( tmp2, vec2(0.25), vec2(0.15) );
    	 col   = mix( col, vec3(0.4, 0.2, 0.2), pct );
    	 pct   = rect( tmp2, vec2(0.29), vec2(0.20) );
    	 col   = mix( col, vec3(0.71), pct );
    	 pct   = circle( tmp2, 0.14, 1.0, 1.0 );
    	 col   = mix( col, vec3(0.98, 0.98, 0.824), pct );
	 	 pct  *= circle( tmp2 - vec2(0.055, 0.0), 0.1, 0.67, 1.37 );
    	 col   = mix( col, vec3(0.71), pct);
    	 pct  *= circle(tmp2 - vec2(0.0), 0.0007, 0.35, 1.1);
    	 col   = mix(col, vec3(0.0), pct);


    	 // floor lights
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );
    	 pct   = circle(st - vec2(0.8, -0.25), 0.62, - 3.0, 2.00);
    	 col   = mix( col, vec3(0.98, 0.98, 0.824), pct );

    	 pct   = circle(st - vec2(-0.8, -0.25), 0.62, - 3.0, 2.00);
    	 col   = mix( col, vec3(0.98, 0.98, 0.824), pct );

    	 pct   = circle(st - vec2(0.0, -0.25), 0.48, - 3.0, 2.00);
    	 col   = mix( col, vec3(0.98, 0.98, 0.824), pct );


         // table
    	 pct   = 1.0 - step( 0.475, tmp2.y );
    	 col   = mix( col, vec3(.055, .475, .616),  pct );


         // table edge
    	 pct   = rect( tmp2 + vec2(0.0, 0.075), vec2(-0.39, -0.50), vec2(0.5, 0.40) );
    	 col   = mix( col, vec3(.02, .32, .32), pct );

    	 pct  *= 1.0 - smoothRect( tmp2 + vec2(0.5, 0.0270), vec2(0.1, 0.1), vec2(0.49999, 0.49999), vec2(0.987, 0.65) );
    	 col   = mix( col, vec3(0.012, 0.176, 0.176), pct );

    	 res   = 0.0, pct = 0.0;
    	 for(int i = 0; i < 12; i++)
         {
             pct += circle( tmp2 - vec2(res - 0.825, 0.0), 0.00025, 0.244, 0.245 );
             res += 0.15;
         }
    	 col  = mix( col, vec3(0.42, 0.75, 0.18),  pct );


         // pockets
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );
    	 tmp2.y = tmp2.y / 0.472 - 0.5;
    	 pct   = circle(tmp2, 0.05, 0.244, 0.245);
    	 res   = rect( st + vec2(0.0, 0.050), vec2(0.445, 0.445), vec2(0.475, 0.475) );
    	 pct  += res - (pct * res);
    	 res   = circle(tmp2 + vec2(0.0, 0.083), 0.05, 0.244, 0.245);
    	 pct  += res - (pct * res);

    	 tmp2  += vec2(0.915, 0.0);
    	 pct  += circle(tmp2, 0.05, 0.244, 0.245);
    	 res   = rect( st + vec2(0.915, 0.050), vec2(0.445, 0.445), vec2(0.475, 0.475) );
    	 pct  += res - (pct * res);
    	 res   = circle(tmp2 + vec2(0.0, 0.083), 0.05, 0.244, 0.245);
    	 pct  += res - (pct * res);

    	 tmp2  -= vec2(1.83, 0.0);
    	 pct  += circle(tmp2, 0.05, 0.244, 0.245);
    	 res   = rect( st + vec2(-0.915, 0.050), vec2(0.445, 0.445), vec2(0.475, 0.475) );
    	 pct  += res - (pct * res);
    	 res   = circle(tmp2 + vec2(0.0, 0.083), 0.05, 0.244, 0.245);
    	 pct  += res - (pct * res);
    	 col   = mix(col, vec3(0.05, 0.05, 0.05), pct);

         // playing cards
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );
    	 tmp2  = tmp2 * 2.0 - 1.0;
    	 pct   = smoothstep( -2.5, 1.5, -tmp2.y );
    	 tmp2 /= pct;

    	 	// shadows -- lower layer
    	 pct   = rect2( rotate(tmp2 - vec2(-1.8, -0.34), -0.5), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-0.87, -0.28), -0.49), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-0.14, -0.36), -0.49), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(0.641, -0.412), -0.51), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(0.687, -0.28), -0.49), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(1.01, -0.34), -0.5), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(1.45, -0.32), -0.49), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(2., -0.34), -0.51), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(0.25), pct);

			// cards -- lower layer
		 pct   = rect2( rotate(tmp2 - vec2(-1.8, -0.34), -0.5), vec2(10.5, 3.5), vec2(0.0), 0.08 );
	     pct  += rect2( rotate(tmp2 - vec2(-0.87, -0.28), -0.49), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-0.14, -0.36), -0.49), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(0.641, -0.412), -0.51), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(0.687, -0.28), -0.49), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(1.01, -0.34), -0.5), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(1.45, -0.32), -0.49), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(2., -0.34), -0.51), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(1.0), pct);

    	 	// shadows -- mid layer
    	 pct   = rect2( rotate(tmp2 - vec2(-1.58, -0.31), -0.5), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-1.1, -0.36), -0.51), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-0.77, -0.38), -0.52), vec2(10.0, 3.45), vec2(0.0), 0.08 );
	 	 pct  += rect2( rotate(tmp2 - vec2(1.67, -0.37), -0.52), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(0.25), pct);

    		// cards -- mid layer
    	 pct   = rect2( rotate(tmp2 - vec2(-1.58, -0.31), -0.5), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-1.1, -0.36), -0.51), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(-0.77, -0.38), -0.52), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 pct  += rect2( rotate(tmp2 - vec2(1.67, -0.37), -0.52), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(1.0), pct);

    		// shadow -- upper
    	 pct   = rect2( rotate(tmp2 - vec2(-0.47, -0.34), -0.51), vec2(10.0, 3.45), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(0.25), pct);

    		// card -- upper
    	 pct   = rect2( rotate(tmp2 - vec2(-0.47, -0.34), -0.51), vec2(10.5, 3.5), vec2(0.0), 0.08 );
    	 col   = mix(col, vec3(1.0), pct);


         // card details	-- |shape_rotation| < |card_rotation|
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );
    	 tmp2  = rotate(tmp2, 0.49);
    	 tmp2  = tmp2 * 29.0;

    		// rotation -- card [50]
    	 pct   = heart(tmp2 - vec2(10.45, 5.95), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(10.95, 3.15), 0.0, 0.0);
      	 pct  += heart(tmp2 - vec2(11.45, -25.65), 0.0, 0.0);

    		// rotation -- card [51]
    	 tmp2  = rotate(tmp2, 0.01);
    	 pct  += heart(tmp2 - vec2(9.75, -21.85), 0.0, 0.0);
		 pct  += heart(tmp2 - vec2(10.65, -37.05), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(10.35, -2.25), 0.0, 0.0);

    		// rotation -- card [52]
    	 tmp2  = rotate(tmp2, -0.03);
    	 pct  += heart(tmp2 - vec2(10.55, -4.75), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(13.35, -32.45), 0.0, 0.0);

    	 	// rotation -- card [49]
    	 tmp2  = rotate(tmp2, 0.04);
    	 pct  += heart(tmp2 - vec2(11.25, -5.35), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(10.0, -13.35), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(10.65, -22.55), 0.0, 0.0);
    	 pct  += heart(tmp2 - vec2(9.95, -31.0), 0.0, 0.0);

    	 col   = mix(col, vec3(0.82, 0.0, 0.0), pct);

    		// spade
		 tmp2  = rotate(tmp2, -0.01);
    	 pct   = heart(tmp2 - vec2(10.65, -8.95), 0.0, 0.0);
    	 pct  += step(lamp( ( rotate(tmp2, 0.25) - vec2(0.70, -14.35)) / 1.33, 3.0), 0.25);
    	 col   = mix(col, vec3(0.0), pct);


         // billiard balls
    	 tmp2  = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );											// 8
    	 tmp2.y = st.y / 0.38 + 0.2;
    	 pct   = circle(tmp2, 0.0041, - 1.65, 2.00);
    	 col   = mix(col, vec3(0.0) + cos(iTime * 2.0) / 2.5, pct);

    	 tmp2  = st / vec2(1.0, 0.38) + vec2( sin(iTime * 100.0) / 500.0, cos(iTime * 100.0) / 500.0 ) - vec2(-0.18, 0.02);
    	 pct   = circle(tmp2, 0.0041, - 1.65, 2.00);
    	 col   = mix(col, vec3(0.0) + cos(iTime * 2.0) / 2.5, pct);

    	 tmp2  = st / vec2(1.0, 0.38) + vec2( sin(iTime * 100.0) / 500.0, cos(iTime * 100.0) / 500.0 ) - vec2(0.4, 0.475);
    	 pct   = circle(tmp2, 0.0041, - 1.65, 2.00);
    	 col   = mix(col, vec3(0.0), pct);

    	 tmp2  = st + vec2( sin(iTime * 100.0) / 500.0, cos(iTime * 100.0) / 500.0 );
    	 pct   = circle(tmp2 + vec2(0.0, 0.35), 0.027, 0.244, 0.245);
    	 col   = mix( col, vec3(0.0), pct );

    	 pct   = circle(tmp2 + vec2(0.18, 0.27), 0.02, 0.244, 0.245);
    	 col   = mix( col, vec3(0.85), pct );
    	 pct   = rect( tmp2 + vec2(0.18, 0.27), vec2(0.465, 0.465), vec2(0.488, 0.488) );
    	 col   = mix( col, vec3(0.7, 0.0, 0.0), pct );

		 pct   = circle(tmp2 + vec2(-0.4, 0.1), 0.018, 0.244, 0.245);
    	 col   = mix( col, vec3(0.85), pct );

    	 tmp2.x = tmp2.x / 0.472 * 1.15;
    	 tmp2.y = tmp2.y / 0.472;
         pct   = circle(tmp2 + vec2(-0.7, 0.16), 0.018, 0.244, 0.245);
    	 col   = mix( col, vec3(0.85), pct );


         // lamp body & movement
     	 tmp2 = st + vec2( sin(iTime * 100.0) / 650.0, cos(iTime * 100.0) / 650.0 );
	     tmp2.x = tmp2.x + sinusoidalTime - 0.5;
    	 tmp2.y = tmp2.y / sin(2.64) - 2.9 + abs(cos(iTime));
    	 pct   = step(lamp(rotate(tmp2, .27 - sin(iTime * 1.0) / 10. ), 3.0), .25);
    	 col   = mix(col, vec3(0.537, 0.502, 0.278), pct);

         // darken everything
    	 col  *= vec3(0.5);

         // lamp's glow & movement
    	 tmp2.x = st.x + sinusoidalTime + sinusoidalTime / 2.5;
    	 tmp2.y = st.y / 0.30;
    	 pct   = circle(tmp2, 0.34, - 2.65, 2.00);
    	 col   = mix( col, vec3(0.98, 0.98, 0.824), pct );

    	 // grain
    	 tmp2  = rotate((tmp2 * 1.5 - vec2(-0.75, 0.0)), iTime) * 20.0 * mod(0.627, 1.0) ;
    	 pct   = smoothstep(0.88, 0.88, noise( tmp2 ));
    	 tmp3  = mix(col, vec3(0.0, 0.0, 0.0), pct);
    	 pct  *= smoothstep(0.81, 0.82, noise( st * 27.0 * mod( (st.x - 12.0) * sin(iTime * 4.8) * cos(iTime * 3.4), 1.0) ));
    	 col   = mix(col, tmp3, pct);

    	 // colorize - grayscale
 		 vec4 c4 = mat4( 0.35,	0.35, 	0.35,  	0.0,
                        0.35, 	0.35, 	0.35, 	0.0,
                        0.35, 	0.35,	0.35, 	0.0,
                        0.0,	0.0,	0.0, 	0.0 ) * vec4(col, 1.0);

    	 //vec4 c4 = vec4( vec3(dot(vec3(0.35), col)), 1.0);		//<< a terse-syntax for the above*/

    	 //vec4 c4 = mat4(1.0) * vec4(col, 1.0);


    fragColor = vec4(c4);
}
