#define PI 3.14159265358

float circle(in vec2 _st, in float _radius){
    vec2 dist = _st-vec2(0.5);
	return 1.-smoothstep(_radius-(_radius*0.08),
                         _radius+(_radius*0.1),
                         dot(dist,dist)*4.0);
}

float ellipse(vec2 _st, float _radius){
    vec2 dist = _st-vec2(0.5);
	return 1.-smoothstep(_radius,
                         _radius,
                         dot(dist,dist / vec2(1.61, 0.67) )*4.0);
}

float rect(vec2 coordSpace, vec2 dim)
{
    vec2 tmp = smoothstep( dim, dim, coordSpace );
    float pct = tmp.x * tmp.y;

    tmp = smoothstep( dim, dim, 1. - coordSpace );
    pct *= tmp.x * tmp.y;

    return pct;
}


float plot(float x, float pct) {

    return step( x, pct ) - step( x, pct - .15 );
}

// by Golan Levin and Collaborators
// http://www.flong.com/texts/code/shapers_exp/
float logisticSigmoid (float x, float a){

  float epsilon = 0.0001;
  float min_param_a = 0.0 + epsilon;
  float max_param_a = 1.0 - epsilon;
  a = max(min_param_a, min(max_param_a, a));
  a = (1./(1.-a) - 1.);

  float A = 1.0 / (1.0 + exp(0. -((x-0.5)*a*2.0)));
  float B = 1.0 / (1.0 + exp(a));
  float C = 1.0 / (1.0 + exp(0.-a));
  float y = (A-B)/(C-B);
  return y;
}

// concept from "Rainbow Showoff"
// by akufishi
// https://www.shadertoy.com/view/lscBRf
float rainDrop( vec2 coordSpace, float scalar, float timeOffset, float modVal)
{
    float tempTime = iTime * 10.;
    float initPos = fract( sin(coordSpace.x) * scalar ) / iTime + timeOffset;
    return rect( vec2(mod(coordSpace.y + tempTime * initPos, modVal)) , vec2(0.485, .44) );
}

float random (in vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))
                * 43758.5453123);
}


// by Patricio Gonzalez Vivo from The Book of Shaders
// https://thebookofshaders.com/11/
float noise(vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);
    vec2 u;
    u = f*f*f*(f*(f*6.-15.)+10.);
    return mix( mix( random( i + vec2(0.0,0.0) ),
                     random( i + vec2(1.0,0.0) ), u.x),
                mix( random( i + vec2(0.0,1.0) ),
                     random( i + vec2(1.0,1.0) ), u.x), u.y);
}

// by Patricio Gonzalez Vivo from The Book of Shaders
// live version below
//     https://thebookofshaders.com/edit.php#11/wood.frag
float lines(in vec2 pos, float b){
    float scale = 10.0;
    pos *= scale;
    return smoothstep(0.0,
                    .5+b*.5,
                    abs((sin(pos.x*3.1415)+b*2.0))*.5);
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    vec2 st = fragCoord/iResolution.xy;

    st -= 0.5;
    st.x *= iResolution.x/iResolution.y;
    st += 0.5;

    vec3 col;
    col = vec3(0.);
    col = vec3(0, .518, 0.694);

    float pct;


    //
    //
    // rain - far layer
    //
    //
    pct = rainDrop(st - vec2(0.0, 0.5), 54873., 0.2, 1.0 );
    col = mix(col, vec3(0.851, 0.851, 1.0), pct );


    //
    //
    // create far hill of grass
    //
    //


    pct = ellipse(st + vec2(-.35, 0.05), .55);
    col = mix(col, vec3(.133, .553, .133), pct);


    //
    //
    // create far river
    //
    //

    float xOffset = 0.68;
    vec2 tmpSpace;

    for (int i = 0; i < 7; i++)
    {
        tmpSpace = st - vec2(xOffset, 0.5025);
        tmpSpace *= vec2(3., 3.);
        float range = step(-0.05, tmpSpace.x) * tmpSpace.y;
        range -= step(0.7, tmpSpace.x) * tmpSpace.y;
        range += step(0.75, tmpSpace.y) * tmpSpace.y;
        range += step(0.8, tmpSpace.y) * tmpSpace.y;

        pct = logisticSigmoid(tmpSpace.x , 0.887);
        pct = plot(range , pct);

        col = mix(col, vec3(0, .576, 0.769), pct );

        xOffset -= 0.01;
    }


    //
    //
    // darken things up to this point
    //
    //
    col = mix(col, vec3(0.0), 0.15);


    //
    //
    // draw the near field of grass
    //
    //

    col = mix(col, vec3(.133, .553, .133), rect(st - vec2(0.0, -.45), vec2(-.5, .02)) );

    //
    //
    // fill the sky of clouds
    //
    //

    float cloudColor = 0.3;
    float yPos = 0.31;
    float width = 0.79;
    float xMod = 0.0;
    float xModOffset = 0.00;

    for (int i = 0; i < 6; i++)
    {
        for (int j = 0; j < 29; j++)
        {
            float mov = 0.0;

            vec2 offset = vec2( -0.95 + xMod + mov + xModOffset, yPos );
            vec2 coords = st * vec2(width, 1.0);

            col = mix(col, vec3(cloudColor), circle(coords - offset, .005) );
            col = mix(col, vec3(cloudColor), circle(coords - offset - vec2(0.06, 0.), .005) );
            col = mix(col, vec3(cloudColor), circle(coords - offset - vec2(-0.06,0.), .005) );

            xMod += .17;
        }

        if (i % 2 == 0)
        {
        	xModOffset = 0.023;
        }
        else
        {
         	xModOffset = -0.013;
        }


        cloudColor += 0.04;
        yPos += 0.025;
        width -= 0.07;

        xMod = 0.0;
    }

    //
    //
    // foliage
    //
    //
    for (int i = 0; i < 5; i++)
    {
        vec2 unitOffset = 	i == 0 ? vec2(0.52, -0.1) :
                            i == 1 ? vec2(0.0, -0.25) :
                            i == 2 ? vec2(-0.5, -0.4) :
                            i == 3 ? vec2(0.43, -.3) :
        							 vec2(-0.45, 0.015);

        float scale = i == 0 ? 0.02 : i == 1 ? 0.025 : i == 2 ? 0.04 : i == 3 ? 0.03 : 0.008;

        vec2 leafOffset = 	i == 0 ? vec2(0.08, 0.0) :
                            i == 1 ? vec2(0.11, 0.0) :
                            i == 2 ? vec2(0.12, 0.0) :
                            i == 3 ? vec2(0.11, 0.0) :
                            		 vec2(0.05, 0.0);


        col = mix(col, vec3(.10, .485, .105), circle( st - vec2(0.0, 0.01) - unitOffset, scale  ) );
        col = mix(col, vec3(.10, .485, .105), circle( st - leafOffset - unitOffset, scale  ) );
        col = mix(col, vec3(.10, .485, .105), circle( st + leafOffset - unitOffset, scale  ) );


        col = mix(col, vec3(.66, .0, .0), circle( st - vec2(0.0, 0.01) - unitOffset - vec2(scale / 0.35, 0.0), scale / 18.5 ) );
        col = mix(col, vec3(.66, .0, .0), circle( st - vec2(0.0, 0.01) - unitOffset - vec2(scale / -0.251, scale / 1.37), scale / 13.5 ) );
        col = mix(col, vec3(.66, .0, .0), circle( st - vec2(0.0, 0.01) - unitOffset - vec2(scale / 0.275, scale / -.53), scale / 15.5 ) );
    }

    //
    //
    // darken everything
    //
    //
    col = mix(col, vec3(0.0, 0., 0.), 0.475);

    //
    //
    // rain - window layer
    //
    //
    pct = rainDrop(st - vec2(0.0, 0.5), 68451., 0.2, 5.5 );
    col = mix(col, vec3(0.851, 0.851, 1.0), pct );

    //
    //
    // periodic lightning strikes
    //
    //

    pct = sin( (iTime * PI) / 1.025) * sin( sqrt( iTime * iTime * PI ) * 0.4 );
    pct = clamp(pct, 0.0, 1.0);

    col = mix(col, vec3(1.0), pct);


    //
    //
    // the window frame
    //
    //

    vec2 pos = st.yx * vec2(2.8, 8.8);
    pos = noise(pos - vec2(0., 20.) ) * pos * 1.5;
    pct = rect(st - vec2(0.0, 0.0), vec2(.48, 0.085) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.35);
    col = mix(col, vec3(.412, .204, .059), pct );


    pos = st.yx * vec2(7.5, 3.8);
    pos = noise(pos - vec2(0., 20.) ) * pos * 1.5;
    pct = rect(st - vec2(0.0, 0.0), vec2(-.30, 0.48) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.354);
    col = mix(col, vec3(.412, .204, .059), pct );


    pos = st.yx * vec2(4.45, 3.3);
    pos = noise(pos) * pos;
    pct = rect(st - vec2(0.0, 0.46), vec2(-.40, .45) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.8);
    col = mix(col, vec3(.412, .204, .059), pct );


    pos = st.yx * vec2(10.2, 5.3);
    pos = noise(pos - vec2(0., 20.) ) * pos * 1.5;
    pct = rect(st - vec2(0.0, -0.46), vec2(-.40, .45) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.4);
    col = mix(col, vec3(.412, .204, .059), pct );


    pos = st.yx * vec2(5.2, 3.5);
    pos = noise(pos - vec2(0., 20.) ) * pos * 1.5;
    pct = rect(st - vec2(0.85, 0.0), vec2(.45, 0.) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.4);
    col = mix(col, vec3(.412, .204, .059), pct );


    pct = rect(st - vec2(-0.85, 0.0), vec2(.45, 0.) );
    col = mix(col, vec3(.545, .271, .075), pct );
    pct *= lines(pos,0.4);
    col = mix(col, vec3(.412, .204, .059), pct );




    fragColor = vec4( col ,1.0);
}
