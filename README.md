# OpenGL_Projects
A collection of self-imposed projects to learn openGL & GLSL with while stepping through various tutorials and guides. Unless stated otherwise, most of these shaders were written to work with the <a href="https://atom.io/packages/glsl-preview">GLSL preview package</a> in ATOM. I make no guarantees that they'll work as-is in your editor of choice.

<h2>000 - Scraps</h2>
A medley of small snippets that usually don't amount to much on their own.

<h2>001 - Game of Life Visualizer</h2>
You can use your mouse to look around. Press space to initiate the sequence. The 'game' will only compute in an imaginary box that spans 5 blocks horizontally and 4 blocks vertically.

<h2>002 - Shaping Functions</h2>
These are featured on <a href="http://www.flong.com">flong.com</a>, and also lightly discussed on <a href="http://thebookofshaders.com">thebookofshaders.com</a>.

<h2>003 - The Book of Shaders</h2>
My answers/work to the exercises found on the <a href="http://thebookofshaders.com">thebookofshaders.com</a>.
