// the book of shaders chaper 7 - shapes
// polar shapes exercises

#ifdef GL_ES
precision mediump float;
#endif

// commented out, for GLSL preview passes these already
//uniform vec2 u_resolution;
//uniform vec2 u_mouse;
//uniform float u_time;

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    vec2 pos = vec2(0.5, 0.5)-st;

    float r = length(pos)*2.0;
    float a = atan(pos.y,pos.x);

    float f = cos(a*3.);
    // f = abs(cos(a*3.));
    // f = abs(cos(a*2.5))*.5+.3;
    // f = abs(cos(a*12.)*sin(a*3.))*.8+.1;
    // f = smoothstep(-.5,1., cos(a*10.))*0.2+0.5;

    color = vec3( 1.-smoothstep(f,f+0.02,r) );

    gl_FragColor = vec4(color, 1.0);
}

/*

//// Animate these shapes.
      CHANGE  vec2 pos = vec2(0.5, 0.5)-st;
      TO      vec2 pos = vec2(cos(u_time), sin(u_time))-st;
      ^ a ccw rotation/ orbit [full circle]

      CHANGE  float f = cos(a*3.);
      TO      float f = cos(a*3. + u_time);
      ^ a ccw spin [note spin is still ccw for sin() as well]

      CHANGE  float r = length(pos)*2.0;
      FOR
              miscellany

//// Combine different shaping functions to cut holes in the shape to make flowers, snowflakes and gears.
      FLOWERS
      float r = length(pos)*0.7;
      float a = atan(pos.y,pos.x);
      float f = abs(cos(a*2.5))*0.1+.18;
      color = vec3( 1.-smoothstep(f,f+0.02,r) );

      r = length(pos)*2.0;
      f = abs(cos(a*2.5))*.5+.3;
      color -= vec3( 1.-smoothstep(f,f+0.02,r) );

      r = length(pos)*6.0;
      f = cos(a*5.);
      color += vec3( 1.-smoothstep(f,f+0.02,r) );

      > inverts the colors (because of te subtraction)
      > creates a highlight/glow affect

      SNOWFLAKES
      float r = length(pos)*3.0;
      float a = atan(pos.y,pos.x);
      float f = cos(a*8.);
      color = vec3( 1.-smoothstep(f,f+0.02,r) );

      r = length(pos)*3.0;
      a = atan(pos.y,pos.x);
      f = abs(cos(a*6.))*.5+.3;

      color -= vec3( 1.-smoothstep(f,f+0.02,r) );
      ^ alternatively,  CHANGE  float f = cos(a*8.);
                        TO      float f = cos(a*80.);

      > the first is a minimal-esque pattern/design
      > the alternate is a no-foreground approach

      GEARS
      float r = length(pos)/2.0;
      float a = atan(pos.y,pos.x);
      float f = cos(a*9.);
      color -= vec3( 1.-smoothstep(f,f+0.02,r) );

      r = length(pos)*2.0;
      a = atan(pos.y,pos.x);
      f = smoothstep(0.,0.1, cos(a*18.))*0.1+0.46;
      color += vec3( 1.-smoothstep(f,f+0.02,r) );

      > this looks more like a wheel than a gear to me


//// Use the plot() function we were using in the Shaping Functions Chapter to draw just the contour.
      float plot (vec2 st, float pct){
          return  smoothstep( pct-0.01, pct, st.y) - smoothstep( pct, pct+0.01, st.y);
      }
      ...
      f = plot(vec2(a, r), f);

      > note the only thing we have to do differently is plot the radius
        > plot normally drew the y-coord on the cartesian plane
      > rememember polar coords are (r,a)

*/
