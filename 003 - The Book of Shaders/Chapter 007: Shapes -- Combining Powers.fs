// the book of shaders chaper 7 - shapes
// combining powers exercises

#define PI 3.14159265359
#define TWO_PI 6.28318530718

// commented out, for GLSL preview passes these already
//uniform vec2 u_resolution;
//uniform vec2 u_mouse;
//uniform float u_time;

// Reference to
// http://thndl.com/square-shaped-shaders.html

void main(){
  vec2 st = gl_FragCoord.xy/u_resolution.xy;
  st.x *= u_resolution.x/u_resolution.y;
  vec3 color = vec3(0.0);
  float d = 0.0;

  // Remap the space to -1. to 1.
  st = st *2.-1.;

  // Number of sides of your shape
  int N = 3;

  // Angle and radius from the current pixel
  float a = atan(st.x,st.y)+PI;
  float r = TWO_PI/float(N);

  // Shaping function that modulate the distance
  d = cos(floor(.5+a/r)*r-a)*length(st);

  color = vec3(1.0-smoothstep(.4,.41,d));
  // color = vec3(d);

  gl_FragColor = vec4(color,1.0);
}

/*

//// Using this example, make a function that inputs the position and number of corners of a desired shape and returns a distance field value.
      SIMPLE APPROACH
      vec4 distToShape(float sides, vec2 pos, float lower, float upper)
      {
          float a=atan(pos.x,pos.y)+.2;
          float b=6.28319/float(sides);
          vec4 f=vec4(vec3(smoothstep(lower, upper, cos(floor(.5+a/b)*b-a)*length(pos.xy))), 1.);
          return f;
      }
      ...
      vec4 f = distToShape(5., st, .2, .9);
      gl_FragColor = vec4(f);

      > using the sample that's defined in the reference
      > just pass in the center [st] and tweak the upper & lower bounds on the
        smoothstep() call
        > parameterized bounds too

//// Mix distance fields together using min() and max().
      vec4 f1 = distToShape(4., st + .2, .1, .9);
      vec4 f2 = distToShape(4., st - .2, .1, .9);

      vec4 f = min(f1, f2);
      //f = max(f1, f2);

      > Mixing two 4-gons together seemed to have the best results
      > Still using the same distToShape() method from before


//// Choose a geometric logo to replicate using distance fields.
      void main(){
        vec2 st = gl_FragCoord.xy/u_resolution.xy;
        st.x *= u_resolution.x/u_resolution.y;
        vec3 color = vec3(0.0);

        // Remap the space to -1. to 1.
        st = st *2.-1.;

        vec4 f1 = vec4(0.0);

        // dark grey emblem
        f1 = distToShape(6., st, .87, .87, .0);
        f1 =  vec4( rgbToPct(44., 44., 44.) + abs(f1.xyz - vec3(1.)), 1.0);

        // green ring
        f1 *= distToShape(6., st, .57, .57, .0);
        f1 = vec4(rgbToPct(140., 203., 131.) * abs(f1.xyz - vec3(1.)), 1.0);

        // white strip
        vec4 f2 = distToShape(6., st, .4, .4, .0);
        f1 += vec4( rgbToPct(140., 203., 131.) * abs(f2.xyz - vec3(1.)), 1.0);

        // inner green hexagon
        f2 = distToShape(6., st, .2, .2, 0.);
        f1 -= vec4( rgbToPct(140., 203., 131.) * abs(f2.xyz - vec3(1.)), 1.0);

        // dark grey triangle
        f1 = vec4(f1.xyz * (distToShape(3., st - vec2(.37, 0.), .2, .2, 0.) ).xyz, 1.);

        gl_FragColor = vec4(f1.xyz, 1.0);
      }

      > note that a 5th parameter was added to the distToShape() method [for rotation]
      > also, a utility method was created so I wouldn't have to lookup RGB % values anymore
      > as for the logo, this is supposed to resemble the Cape Crucible's emblem [https://www.capecrucible.org/]
        > colors are off, though
      > as a final note, pos.x & pos.y were switched in var a for distToShape(). ie
        BEFORE  a = atan(pos.x, pos.y) + .2;
        AFTER   a = atan(pos.y, pos.x) + rotationParameter;
*/
