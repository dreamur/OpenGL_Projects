// the book of shaders chaper 7 - shapes
// circles exercises

#ifdef GL_ES
precision mediump float;
#endif

void main(){
	vec2 st = gl_FragCoord.xy/u_resolution;
    float pct = 0.0;

    // a. The DISTANCE from the pixel to the center
    pct = distance(st,vec2(0.5));

    // b. The LENGTH of the vector
    //    from the pixel to the center
    // vec2 toCenter = vec2(0.5)-st;
    // pct = length(toCenter);

    // c. The SQUARE ROOT of the vector
    //    from the pixel to the center
    // vec2 tC = vec2(0.5)-st;
    // pct = sqrt(tC.x*tC.x+tC.y*tC.y);

    vec3 color = vec3( pct );

	gl_FragColor = vec4( color, 1.0 );
}


/*
//// Use step() to turn everything above 0.5 to white and everything below to 0.0.
        CHANGE  vec3 color = vec3( pct );
        TO      vec3 color = vec3( step(pct, 0.5) );

//// Inverse the colors of the background and foreground.
        CHANGE  vec3 color = vec3( pct );
        TO      vec3 color = vec3( 1.0 - step(pct, 0.5) );

//// Using smoothstep(), experiment with different values to get nice smooth borders on your circle.
        CHANGE  vec3 color = vec3( pct );
        TO      vec3 color = vec3( smoothstep(0.3, .5, pct) );

//// Once you are happy with an implementation, make a function of it that you can reuse in the future.
        vec3 smoothCircle(float lowBound, float upBound, float var)
        {
            return vec3(smoothstep(lowBound, upBound, var));
        }


//// Add color to the circle.
        CHANGE  vec3 color = vec3( pct );
        TO      vec3 blue = vec3(0.0, 0.0, 1.0);
        	    vec3 color = step(0.5, smoothCircle(0.1, 0.3, pct));
                color = step(vec3(0.5), mix(color, blue, .5));
                gl_FragColor = vec4( color, 1.0 );


////    Can you animate your circle to grow and shrink, simulating a beating heart? (You can get some inspiration from the animation in the previous chapter.)
        CHANGE  vec3 color = vec3( pct );
        TO      vec3 color = step(0.5,
                                  smoothCircle(0.05,
                                                clamp( (sin(u_time) * sin(u_time)) *2.4, 0.05, 1.0),
                                                pct
                                               )
                                  );



////    What about moving this circle? Can you move it and place different circles in a single billboard?
        CHANGE  pct = distance(st, vec2(0.5));
        TO      pct = distance(st, vec(x, y));
                ^ do this for each circle

                then multiply resultant color vectors together like so

                final_color = circle_1_color * circle_2_color * ... * circle_n_color
                gl_FragColor(final_color, 1.0); <- this will draw them all

////    What happens if you combine distances fields together using different functions and operations?

        pct = distance(st,vec2(0.4)) + distance(st,vec2(0.6));
        > the fields are combined

        pct = distance(st,vec2(0.4)) * distance(st,vec2(0.6));
        > the magnitude of the fields are multiplied/expanded

        pct = min(distance(st,vec2(0.4)),distance(st,vec2(0.6)));
        > slightly emphasizes fields
        > creates 'beam' betwtween two points where distances are equal

        pct = max(distance(st,vec2(0.4)),distance(st,vec2(0.6)));
        > inverse of min

        pct = pow(distance(st,vec2(0.4)),distance(st,vec2(0.6)));
        > the points are heavily emphasized
        > 0.4 is dark/ 0.6 is light
        > rest of field is nearly all same

*/
