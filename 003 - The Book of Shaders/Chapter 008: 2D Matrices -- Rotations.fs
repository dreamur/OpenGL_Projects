// the book of shaders chaper 8 - 2D matrices
// rotations exercises

// Credit to @patriciogv ( patriciogonzalezvivo.com ) - 2015
#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159265359

// commented out, for GLSL preview passes these already
//uniform vec2 u_resolution;
//uniform float u_time;

mat2 rotate2d(float _angle){
    return mat2(cos(_angle),-sin(_angle),
                sin(_angle),cos(_angle));
}

float box(in vec2 _st, in vec2 _size){
    _size = vec2(0.5) - _size*0.5;
    vec2 uv = smoothstep(_size,
                        _size+vec2(0.001),
                        _st);
    uv *= smoothstep(_size,
                    _size+vec2(0.001),
                    vec2(1.0)-_st);
    return uv.x*uv.y;
}

float cross(in vec2 _st, float _size){
    return  box(_st, vec2(_size,_size/4.)) +
            box(_st, vec2(_size/4.,_size));
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    // move space from the center to the vec2(0.0)
    st -= vec2(0.5);
    // rotate the space
    st = rotate2d( sin(u_time)*PI ) * st;
    // move it back to the original place
    st += vec2(0.5);

    // Show the coordinates of the space on the background
    // color = vec3(st.x,st.y,0.0);

    // Add the shape on the foreground
    color += vec3(cross(st,0.4));

    gl_FragColor = vec4(color,1.0);
}

/*

//// Use rotations to improve the animation you simulated in the translation exercise.
    WATER TRANSLATION

    st -= vec2(0.5);
    vec2 translate = vec2(sin(u_time*1.), abs(cos(u_time*2.5)));
    st += translate*0.35;
    st = rotate2d( sin(u_time*8.)/2. ) * st;
    st += vec2(0.5);

    > note that we still move the coord space back to the origin
      > but then we calculate the translation and apply it (before the rotation)
      > note the translation is not removed

    PING PONG BALL

    st -= vec2(0.5);
    vec2 translate = vec2(sin(u_time*9.), -abs(cos(u_time*7.))+1. );
    st += translate*0.35;
    st = rotate2d( -cos(u_time*8.)*4. ) * st;
    st += vec2(0.5);

    HOVER EFFECT

    st -= vec2(0.5);
    vec2 translate = vec2(0.0, clamp(exp(-cos(u_time)*cos(u_time)), 0., 1.));
    st += translate*0.35;
    st = rotate2d( -cos(u_time*.2)*.07 ) * st;
    st += vec2(0.5);

*/
