// the book of shaders chaper 8 - 2D matrices
// translations exercise

#ifdef GL_ES
precision mediump float;
#endif

// commented out, for GLSL preview passes these already
//uniform vec2 u_resolution;
//uniform float u_time;

float box(in vec2 _st, in vec2 _size){
    _size = vec2(0.5) - _size*0.5;
    vec2 uv = smoothstep(_size,
                        _size+vec2(0.001),
                        _st);
    uv *= smoothstep(_size,
                    _size+vec2(0.001),
                    vec2(1.0)-_st);
    return uv.x*uv.y;
}

float cross(in vec2 _st, float _size){
    return  box(_st, vec2(_size,_size/4.)) +
            box(_st, vec2(_size/4.,_size));
}

void main(){
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec3 color = vec3(0.0);

    // To move the cross we move the space
    vec2 translate = vec2(cos(u_time),sin(u_time));
    st += translate*0.35;

    // Show the coordinates of the space on the background
    // color = vec3(st.x,st.y,0.0);

    // Add the shape on the foreground
    color += vec3(cross(st,0.25));

    gl_FragColor = vec4(color,1.0);
}

/*

//// Using u_time together with the shaping functions move
//// the small cross around in an interesting way.
//// Search for a specific quality of motion you are interested in
//// and try to make the cross move in the same way.
//// Recording something from the "real world" first might be useful -
//// it could be the coming and going of waves, a pendulum movement, a bouncing ball,
//// a car accelerating, a bicycle stopping.
    CHANGE  vec2 translate = vec2(cos(u_time),sin(u_time));
    TO      vec2 translate = vec2(sin(u_time*1.), abs(cos(u_time*2.5)) );

    > reminds me of a bobber, riding on the waves

    CHANGE  vec2 translate = vec2(cos(u_time),sin(u_time));
    TO      vec2 translate = vec2(sin(u_time*9.), -abs(cos(u_time*7.))+1. );

    > reminds me of a heated ping-pong rally

    CHANGE  vec2 translate = vec2(cos(u_time),sin(u_time));
    TO      vec2 translate = vec2(0.0, clamp(exp(-cos(u_time)*cos(u_time)), 0., 1.));

    > makes a cool hover effect

*/
