/***
 *
 * This file was structured largely off of the code
 *  written by Joey de Vries at learnopengl.com
 *
 * I take next to no credit for what I've written in
 *  in this file, for I'm still learning
 *
 **/

#include <iostream>
#include <thread>

#define GLEW_STATIC
#include <glew.h>
#include <glfw3.h>

#include <Shader.h>
#include <camera.h>
#include <life.h>

#include <soil/SOIL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void do_movement();
void process_next_frame(GLuint modelLoc);

int WIDTH = 800, HEIGHT = 600;
GLfloat lastX = WIDTH / 2.0f, lastY = HEIGHT / 2.0f;

Camera camera(glm:: vec3(0.0f, 0.0f, 3.0f));

GLfloat deltaTime = 0.0005f;

bool keys[1024];

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

    GLFWwindow* window = glfwCreateWindow(800, 600, "Game o' Life", nullptr, nullptr);

    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glewExperimental = GL_TRUE;
    glewInit();

    glViewport(0, 0, WIDTH, HEIGHT);

    Life_Handler Conway_object;
    Conway_object.Generate_Initial_Positions(GLIDER);
    Shader shaderObject("_vector_life_forms.txt", "_fragment_life_forms.txt");

    GLfloat vertices[] = {
        -0.5f, -0.5f,  0.0f,  0.0f, 0.0f,
         0.5f, -0.5f,  0.0f,  1.0f, 0.0f,
         0.5f,  0.5f,  0.0f,  1.0f, 1.0f,
         0.5f,  0.5f,  0.0f,  1.0f, 1.0f,
        -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
        -0.5f, -0.5f,  0.0f,  0.0f, 0.0f,

    };

    GLuint VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0);


    /* ---- TEXTURE ---- */

    GLuint life_tex;
    glGenTextures(1, &life_tex);

    glBindTexture(GL_TEXTURE_2D, life_tex);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int texW, texH;
    unsigned char* image = SOIL_load_image("D:/Code/OpenGL/includes/images/awesomeface.png", &texW, &texH, 0, SOIL_LOAD_RGB);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texW, texH, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_2D);

    SOIL_free_image_data(image);

    glBindTexture(GL_TEXTURE_2D, 0);


    /* ---- LOOP ---- */
    using namespace std:: chrono_literals;

    while (!glfwWindowShouldClose(window))
    {
        glfwPollEvents();
        do_movement();

        glClearColor(0.2f, 0.1f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        shaderObject.Use();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, life_tex);
        glUniform1i(glGetUniformLocation(shaderObject.Program, "ourTexture1"), 0);

        glm:: mat4 view, projection;

        view = camera.GetViewMatrix();
        projection = glm:: perspective(camera.Zoom, (GLfloat)WIDTH/ (GLfloat)HEIGHT, 0.1f, 100.0f);

        GLuint modelLoc = glGetUniformLocation(shaderObject.Program, "model");
        GLuint viewLoc  = glGetUniformLocation(shaderObject.Program, "view");
        GLuint projLoc  = glGetUniformLocation(shaderObject.Program, "projection");

        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm:: value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm:: value_ptr(projection));


        glBindVertexArray(VAO);

        if (keys[GLFW_KEY_SPACE])
        {
            for (int i = 0; i < 80; i++) {

                Conway_object.Mark_Future_State(Conway_object.life_list[i]);
            }

            for (int i = 0; i < 80; i++) {

                if (Conway_object.Update_Current_State(Conway_object.life_list[i]))
                {
                    // boundaries are +/- 5.0f, +/- 4.0f
                    glm:: mat4 model;
                    model = glm:: translate(model, Conway_object.life_list[i].position);
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm:: value_ptr(model));
                    glDrawArrays(GL_TRIANGLES, 0, 6);
                }

            }
            std:: this_thread:: sleep_for(0.2s);
        }
        else
        {
            for (int i = 0; i < 80; i++) {

                if (Conway_object.Return_Current_State(Conway_object.life_list[i]))
                {
                    glm:: mat4 model;
                    model = glm:: translate(model, Conway_object.life_list[i].position);
                    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm:: value_ptr(model));
                    glDrawArrays(GL_TRIANGLES, 0, 6);
                }
            }
        }

        /* note that each model matrix fits w/in a 1.0f x 1.0f box. */
        glBindVertexArray(0);
        glfwSwapBuffers(window);

    }

    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    glfwTerminate();

    return 0;
}

void do_movement()
{

    if (keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

bool firstMouse = true;
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if (key == GLFW_KEY_ESCAPE) glfwSetWindowShouldClose(window, GL_TRUE);

    if (action == GLFW_PRESS)
        keys[key] = true;
    else if (action == GLFW_RELEASE)
        keys[key] = false;
}
