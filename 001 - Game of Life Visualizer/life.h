/***
 *
 * this is where the real magic happened.
 *
 **/

#ifndef __LIFE_H
#define __LIFE_H

#include <vector>
#include <glew.h>
#include <glm/glm.hpp>
#include <cmath>

// this is here to make swapping out the initial pattern easier
enum initial_seeds {
	GLIDER,
	FLIPPER
};

class Life
{
	public:
		bool current_state, future_state;
		glm:: vec3 position;

		Life()
		{
		    this->current_state     = false;
		    this->position          = glm:: vec3(0.0f, 0.0f, 0.0f);
		    this->future_state      = false;
		}

		Life(glm:: vec3 param_current_pos)
		{
			this->current_state 	= true;
			this->position 		    = param_current_pos;
			this->future_state      = true;
		}
		Life(glm:: vec3 param_current_pos, bool state)
		{
			this->current_state 	= state;
			this->position 		    = param_current_pos;
			this->future_state      = false;
		}
};

class Life_Handler
{
	public:
		std:: vector <Life> life_list;

		Life_Handler()
		{
		    this->life_list.resize(80);
            int index = 0;
            GLfloat X = -5.0f, Y = 4.0f;
            for (int i = 0; i < 8; i++)
            {
                for (int k = 0; k < 10; k++)
                {
                    this->life_list[index] = Life(glm:: vec3(X, Y, -5.0f), false);
                    index++;
                    X += 1.0f;
                }
                X = -5.0f;
                Y -= 1.0f;
            }
		}

		GLvoid Generate_Initial_Positions(initial_seeds param_chosen_seed)
		{
			if (param_chosen_seed == GLIDER)
			{
				this->life_list[0]  = Life (glm:: vec3(-5.0f, 4.0f, -5.0f));
				this->life_list[2]  = Life (glm:: vec3(-3.0f, 4.0f, -5.0f));
				this->life_list[11] = Life (glm:: vec3(-4.0f, 3.0f, -5.0f));
				this->life_list[12] = Life (glm:: vec3(-3.0f, 3.0f, -5.0f));
				this->life_list[21] = Life (glm:: vec3(-4.0f, 2.0f, -5.0f));
			}
			else if (param_chosen_seed == FLIPPER)
            {
                this->life_list[10] = Life (glm:: vec3(-5.0f, 3.0f, -5.0f));
				this->life_list[11] = Life (glm:: vec3(-4.0f, 3.0f, -5.0f));
				this->life_list[12] = Life (glm:: vec3(-3.0f, 3.0f, -5.0f));
            }
		}

		GLuint Count_Neighbors(Life& organism)
		{
			GLfloat X = organism.position.x;
			GLfloat Y = organism.position.y;

			GLuint neighbors = 0;

			for (int i = 0; i < 80; i++)
			{
				// NW
				if (this->life_list[i].position.x == (X + -1) && this->life_list[i].position.y == (Y + 1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// N
				else if (this->life_list[i].position.x == X && this->life_list[i].position.y == (Y + 1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// NE
				else if (this->life_list[i].position.x == (X + 1) && this->life_list[i].position.y == (Y + 1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// W
				else if (this->life_list[i].position.x == (X + -1) && this->life_list[i].position.y == Y)
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// E
				else if (this->life_list[i].position.x == (X + 1) && this->life_list[i].position.y == Y)
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// SW
				else if (this->life_list[i].position.x == (X + -1) && this->life_list[i].position.y == (Y + -1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// S
				else if (this->life_list[i].position.x == X && this->life_list[i].position.y == (Y + -1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }
				// SE
				else if (this->life_list[i].position.x == (X + 1) && this->life_list[i].position.y == (Y + -1))
                {
                    if(this->life_list[i].current_state == true)
						neighbors++;
                }

			}
			return neighbors;
		}

		GLvoid Mark_Future_State(Life& organism)
		{
			GLuint neighbors = Count_Neighbors(organism);
			if (organism.current_state == true)
			{
				if(neighbors < 2) 		organism.future_state = false;
				else if (neighbors > 3) organism.future_state = false;
				else					organism.future_state = true;
			}
			else
			{
				if (neighbors == 3)		organism.future_state = true;
				else					organism.future_state = false;
			}
		}

		bool Update_Current_State(Life& organism)
		{
			organism.current_state = organism.future_state;
			return organism.current_state;
		}

		bool Return_Current_State(const Life& organism)
		{
			return organism.current_state;
		}
};

#endif
